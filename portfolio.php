<?php
// include 'admin/scripts/connect.php';
include 'lightbox.php';
?>
<!-- nav start -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>cammata</title>
</head>
<body>
  <!-- nav start -->
  <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"><img class="hidden-sm hidden-xs img-responsive" src="images/logo-small.png" alt="small logo"></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="index.php">HOME</a></li>
          <li><a href="about.php">ABOUT</a></li>
          <li><a href="portfolio.php">PORTFOLIO</a></li>
          <li><a href="index.php">CONTACT</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

<!-- nav end -->
<!-- portfolio page -->
<div class="container-fluid" id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-xs-10 col-sm-10 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
        <h2>Portfolio</h2>
      </div>
    </div>

    <div class=" row portfolio">
      <div class=" col-xs-6 col-sm-6 col-md-4 col-lg-4 im1">
        <img id="3" class="portImg img-responsive im1" src="images/thumbs/macrita-thumb.jpg" alt="thumbnail">
      </div>
      <div class=" col-xs-6 col-sm-6 col-md-4 col-lg-4 im2">
        <img id="5" class="portImg img-responsive" src="images/thumbs/arial-poster-thumb.jpg" alt="thumbnail">
      </div>
      <div class=" col-xs-6 col-sm-6 col-md-4 col-lg-4 im1">
        <img id="9" class="portImg img-responsive im2" src="images/thumbs/oil-light-thumb.jpg" alt="thumbnail">
      </div>
      <div class=" col-xs-6 col-sm-6 col-md-4 col-lg-4 im2">
        <img id="11" class="portImg img-responsive " src="images/thumbs/momantai-dress-thumb.jpg" alt="thumbnail">
      </div>
      <div class=" col-xs-6 col-sm-6 col-md-4 col-lg-4 im1">
        <img id="13" class="portImg img-responsive im2" src="images/thumbs/wine-poster-thumb.jpg" alt="thumbnail">
      </div>
      <div class=" col-xs-6 col-sm-6 col-md-4 col-lg-4 im2">
        <img id="15" class="portImg img-responsive im1" src="images/thumbs/sports-logo-thumb.jpg" alt="thumbnail">
      </div>
      <div class=" col-xs-6 col-sm-6 col-md-4 col-lg-4 im1">
        <img id="19" class="portImg img-responsive im2" src="images/thumbs/september-thumb.jpg" alt="thumbnail">
      </div>
      <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 im2">
        <img id="7" class="portImg img-responsive im1" src="images/thumbs/foco-thumb.jpg" alt="thumbnail">
      </div>
      <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 im1">
        <img id="17" class="portImg img-responsive" src="images/thumbs/humo-thumb.jpg" alt="thumbnail">
      </div>
      <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 im1">
        <a href="lazy/index.html"><img class="portImg img-responsive" src="images/thumbs/lazy-student-thumb.jpg" alt="thumbnail"></a>
      </div>
    </div>
  </div>
</div>
<div class="lightbox hidden">
 <div class="container-fluid">
   <div class="row">
     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-10 col-sm-offset-10 col-md-offset-10 col-lg-offset-10">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
     </div>
   </div>
 </div>
  <div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <img class="img-responsive lightbox-img" src="" alt="lightbox image">
      <video class="lightbox-img" src="" controls>

      </video>
    </div>
  </div>
</div>
</div>
<!-- footer start -->
<footer class="container-fluid">
  <div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <ul>
        <li><a href="index.php">HOME</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
        <li><a href="index.php">ABOUT</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
        <li><a href="portfolio.php">PORTFOLIO</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="index.php">CONTACT</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-md-offset-6 col-lg-offset-6">
      <div class="row atright">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://www.facebook.com/cam.mata.1" class="fa fa-facebook-square fa-3x" aria-hidden="true"></a>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://www.instagram.com/kmi_mata/" class="fa fa-instagram fa-3x" aria-hidden="true"></a>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://twitter.com/kmimata19" class="fa fa-twitter fa-3x" aria-hidden="true"></a>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://www.linkedin.com/in/camila-mata-salinas-81759b117/" class="fa fa-linkedin-square fa-3x" aria-hidden="true"></a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 colo-lg-12 copyright">
          <h4>&copy; Camila Mata 2018</h4>
        </div>
      </div>
    </div>
  </div>
</div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="js/main.js"></script>

</body>
</html>

<!-- footer end -->
