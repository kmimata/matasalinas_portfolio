<?php
 ?>

 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <title>cammata</title>
</head>
<body>
<!-- nav start -->
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="index.php">HOME</a></li>
        <li><a href="index.php">ABOUT</a></li>
        <li><a href="portfolio.php">PORTFOLIO</a></li>
        <li><a href="index.php">CONTACT</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!-- nav end -->

  <!-- thanks start -->
  <div class="container-fluid" id="thankyou">
    <div class="container">
      <div class="row circle-part">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 circleYat">
          <h1>Thank You Very Much!</h1>
          <h3>I'll get back to you as fast as I can, so don't stress out</h3>
        </div>
      </div>
  </div>
</div>
<!-- thanks end -->

<!-- footer start -->
<footer class="container-fluid foterthank">
  <div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <ul>
        <li><a href="index.php">HOME</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
        <li><a href="index.php">ABOUT</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
        <li><a href="portfolio.php">PORTFOLIO</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="index.php">CONTACT</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-md-offset-6 col-lg-offset-6">
      <div class="row atright">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <i href="https://www.facebook.com/cam.mata.1" class="fa fa-facebook-square fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <i href="https://www.instagram.com/kmi_mata/" class="fa fa-instagram fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <i href="https://twitter.com/kmimata19" class="fa fa-twitter fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <i href="https://www.linkedin.com/in/camila-mata-salinas-81759b117/" class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 colo-lg-12 copyright">
          <h4>&copy; Camila Mata 2018</h4>
        </div>
      </div>
    </div>
  </div>
</div>
</footer>
<!-- footer end -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
