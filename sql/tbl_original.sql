-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Host: 168.243.235.122
-- Generation Time: Jan 12, 2018 at 10:06 AM
-- Server version: 10.2.6-MariaDB
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `c136kmimata`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_original`
--

CREATE TABLE IF NOT EXISTS `tbl_original` (
  `original_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `original_name` varchar(70) NOT NULL,
  `original_thumb` varchar(70) NOT NULL,
  PRIMARY KEY (`original_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_original`
--

INSERT INTO `tbl_original` (`original_id`, `original_name`, `original_thumb`) VALUES
(3, 'macrita.jpg', 'macrita-thumb.jpg'),
(5, 'arial-poster.jpg', 'arial-poster-thumb.jpg'),
(7, 'foco.jpg', 'foco-thumb.jpg'),
(9, 'oil-light.jpg', 'oil-light-thumb.jpg'),
(11, 'momantai-dress.jpg', 'momantai-dress-thumb.jpg'),
(13, 'wine-poster.jpg', 'wine-poster-thumb.jpg'),
(15, 'sports-logo.jpg', 'sports-logo-thumb.jpg'),
(17, 'humo.jpg', 'humo-thumb.jpg'),
(19, 'september.jpg', 'september-thumb.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
