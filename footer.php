<?php
?>

<footer class="container-fluid">
  <div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <ul>
        <li><a href="#logo">HOME</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
        <li><a href="#about">ABOUT</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
        <li><a href="#">PORTFOLIO</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="#contact">CONTACT</a></li>
        <li role="separator" class="hidden-md hidden-lg divider"></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-md-offset-6 col-lg-offset-6">
      <div class="row atright">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://www.facebook.com/cam.mata.1" class="fa fa-facebook-square fa-3x" aria-hidden="true"></a>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://www.instagram.com/kmi_mata/" class="fa fa-instagram fa-3x" aria-hidden="true"></a>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://twitter.com/kmimata19" class="fa fa-twitter fa-3x" aria-hidden="true"></a>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 small-icon">
          <a href="https://www.linkedin.com/in/camila-mata-salinas-81759b117/" class="fa fa-linkedin-square fa-3x" aria-hidden="true"></a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 colo-lg-12 copyright">
          <h4>&copy; Camila Mata 2018</h4>
        </div>
      </div>
    </div>
  </div>
</div>
</footer>

<script src="js/main.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
