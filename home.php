<?php
?>

<div class="container-fluid">
<div class="container">
<div class="row">
  <div class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-3 col-md-offset-4 col-lg-3 col-lg-offset-4" id="logo">
    <img class="img-responsive" src="images/logo.png" alt="logo">
  </div>
</div>
</div>
</div>
<!-- nav end -->

<!-- header start -->
<div class="container-fluid">
  <!-- <div class="container"> -->
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 banner">
      <img class="img-responsive banner-img hidden-xs hidden-sm" src="images/banner.jpg" alt="castle by the sea">
      <img class="img-responsive banner-img hidden-md hidden-lg" src="images/banner-small.jpg" alt="castle by the sea">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h2>Welcome!</h2>
          </div>
        </div>
      </div>
  </div>
</div>
<!-- </div> -->
</div>

<!-- header end -->
<!-- content -->
<div class="container-fluid">
<div class="container">
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="about">
    <p>
    Do you want to learn who am I? Do you want to see what I'm capable of? Well, why not take a look at my portfolio and get an idea of what I am.
    <br>
    You see, the first thing you need to know about me is I am a student from El Salvador and have studied basically all my life in El Salvador. Graduated from high school in 2015 and had a year of Strategic Design at Monica Herrera. However, ever since I came to Canada in 2017 I’ve been studying Interactive Media Design and furthering more my knowledge in design and webpage coding.
    </p>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <img class="img-responsive" src="images/picture.png" alt="">
  </div>
</div>
<!-- skills start -->
<div class="row" id="skill">
  <h2>Skills</h2>
</div>
<div class="row skills">
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
      <img class="img-responsive" src="images/icon/psd.png" alt="adobe Photoshop">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
      <img class="img-responsive" src="images/icon/ai.png" alt="adobe illustrator">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
      <img class="img-responsive" src="images/icon/id.png" alt="adobe in design">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
      <img class="img-responsive" src="images/icon/dw.png" alt="adobe dream weaver">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 col-xl-2 ">
      <img class="img-responsive" src="images/icon/ae.png" alt="adobe after effects">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
      <img class="img-responsive" src="images/icon/pr.png" alt="adobe premiere pro">
    </div>
  </div>
</div>
<div class="row ">
  <div class="col-xs-4 col-sm-4 col-md-1 col-md-offset-4 col-lg-1 col-lg-offset-4 col-xl-1 col-xl-offset-4 col-xs-offset-1 col-sm-offset-1">
    <img class="img-responsive" src="images/icon/html.png" alt="html">
  </div>
  <div class="col-xs-4 col-sm-4 col-md-1 col-lg-1 col-xl-1">
    <img class="img-responsive" src="images/icon/css.png" alt="css">
  </div>
</div>
<!-- skills end -->
<!-- portfolio start -->
<div class="row" id="portfolio">
  <h2><a href="portfolio.php">Portfolio</a></h2>
</div>

      <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Carousel indicators -->
          <!-- Wrapper for carousel items -->
          <div class="carousel-inner">
              <div class="item active">
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                    <img class="img-responsive" src="images/thumbs/macrita-thumb.jpg" alt="photography">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                    <img class="img-responsive" src="images/thumbs/graphic-design.jpg" alt="graphic design">
                  </div>
                  <div class="hidden-xs hidden-sm col-md-4 col-lg-4 col-xl-4">
                    <img class="img-responsive" src="images/thumbs/3d design" alt="3d design">
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                    <img class="img-responsive" src="images/thumbs/macrita-thumb.jpg" alt="photography">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                    <img class="img-responsive" src="images/thumbs/graphic-design.jpg" alt="graphic design">
                  </div>
                  <div class="hidden-xs hidden-sm col-md-4 col-lg-4 col-xl-4">
                    <img class="img-responsive" src="images/thumbs/3d-design.jpg" alt="3d design">
                  </div>
                </div>
              </div>
          </div>

          <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
          </ol>

          <!-- Carousel controls -->
          <a class="carousel-control left" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="carousel-control right" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
      </div>
<!-- portfolio end -->
</div>
</div>
<!-- content end -->
