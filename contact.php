<?php
    require_once("admin/scripts/config.php");
    if(isset($_POST['name'])){
      $name = $_POST['name'];
      $email = $_POST['email'];
      $phone = $_POST['phone'];
      $message = $_POST['message'];
      $street = $_POST['street'];
      $direct = "thankyou.php"; //<-- page for answer message after contact has been sent

      if($street == "") {
        $sendMail = submitMessage($name, $email, $message, $phone, $direct);
        // echo "street is empty";
      // }else{
        // echo "street was filled out";
      }
    }

?>
<div class="container-fluid" id="contact">
  <div class="container">
    <div class="row contHead">
      <div class="col-xs-9 col-sm-9 col-md-6 col-lg-6">
        <h2>Mind to talk?</h2>
      </div>
    </div>
    <div class="row ">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3>How about we have a little chichat, shall we? Nobody got hurt by contacting someone else</h3>
      </div>
    </div>
    <form action="contact.php" method="post">
      <div class="form-group">
        <!--Make sure to give each input a name attribute(name="")-->
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label>Name:</label>
            <input required name="name" type="text" size="21" maxlength="30" placeholder="Here goes your name"/>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <label>Email:</label>
           <input required name="email" type="email" size="21" maxlength="30" placeholder="Now your email"/>
         </div>
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <label>Phone number:</label>
           <input name="phone" type="text" size="21" maxlength="30" placeholder="Optional field"/>
         </div>
         <div class="hidden col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <label class="hidden" for="street">Street: </label>
           <input class="hidden street" name="street" type="text" size="21" maxlength="30" />
         </div>
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <label required for="message">Message:</label>
         </div>
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <textarea placeholder="We need something to start the conversation" name="message"></textarea>
         </div>
         <div class="">
           <input id="submit" name="submit" type="submit" value="Send" />
         </div>
   </div>
</div>
</div>
</div>
