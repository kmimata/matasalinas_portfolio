<?php

// this file will connect us to the database
$user = "";
// $pass = "root"; // Mac platform
$pass = ""; //in PC
$url = "";
$db = "";

$link = mysqli_connect($url, $user, $pass, $db); // PC
//$link = mysqli_connect($url, $user, $pass, $db, "8889")
//echo json_encode ($link);
if(!$link){
   echo "try to reconnect";
 }


// check connection error
if(mysqli_connect_errno()){
  printf("Connect failed: %s\n", mysqli_connect_error());
  exit(); //forces an exit just in case things get caught in a loop
}

// cambiamos de tabla porque usuarios_test es InnoDB y la otra era MyISAM
$mysqlQuery = "SELECT original_name FROM tbl_original";

$result = mysqli_query($link, $mysqlQuery);

// este array lo creamos para poder guardar las filas de informacion y poderlas accesar
// de manera mas facil
$emparray = array ();

// Era mysqli_fetch_ARRAY el metodo correcto!!!!!
// ojo, mysqli_fetch_ARRAY, mira aqui abajo:
//
while ($resultado = mysqli_fetch_array($result)){
//	echo $resultado;
	$emparray[] = $resultado;
}


//echo json_decode($emparray);
//echo json_encode($result);
//echo "<br>";
//echo sizeof ($emparray);

// $path_images = "../../images/originals/";
//
// for ($a = 0; $a < count ($emparray); $a++) {
// 	echo "<img src=\"";
// 	echo $path_images;
// 	echo $emparray[$a]["original_name"];
// 	echo "\">";
// //	echo $a;
// }

mysqli_close($link);

?>
